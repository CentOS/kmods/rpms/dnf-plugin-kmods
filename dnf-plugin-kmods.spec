Name:             dnf-plugin-kmods
Version:          1
Release:          1%{?dist}
Summary:          DNF plugin to improve handling kABI tracking kmods
URL:              https://gitlab.com/CentOS/kmods/rpms/dnf-plugin-kmods
License:          GPLv2

Source0:          GPL-2.0
Source1:          kmods.py
Source2:          kmods.conf

BuildArch:        noarch

BuildRequires:    python3-devel
BuildRequires:    python3-dnf


%description
Source package for DNF plugin to improve handling kABI tracking kmod packages.


%package -n python3-%{name}
Summary:          DNF plugin to improve handling kABI tracking kmods - Python3
Requires:         python3-dnf


%description -n python3-%{name}
Installing this package enables a DNF plugin which improves handling kABI
tracking kmod packages. By default only warnings about missing compatible
versions of installed kmod packages for current running and latest installed
kernel are emitted. The plugin can be configured to prevent updating the kernel
before updated versions of all installed kmod packages are available.


%prep


%build


%install
%{__install} -m 644 -D -t %{buildroot}%{_datadir}/licenses/python3-%{name} %{SOURCE0}
%{__install} -m 644 -D -t %{buildroot}%{python3_sitelib}/dnf-plugins %{SOURCE1}
%{__install} -m 644 -D -t %{buildroot}%{_sysconfdir}/dnf/plugins %{SOURCE2}


%files -n python3-%{name}
%license GPL-2.0
%{python3_sitelib}/dnf-plugins/kmods.py
%{python3_sitelib}/dnf-plugins/__pycache__/kmods.*
%config(noreplace) %{_sysconfdir}/dnf/plugins/kmods.conf


%changelog
* Fri Jun 17 2022 Peter Georg <peter.georg@physik.uni-regensburg.de> - 1-1
- Initial version
