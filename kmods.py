# SPDX-License-Identifier: GPL-2.0
#
# Copyright (C) 2022-2022 Peter Georg
#

from __future__ import absolute_import
from __future__ import unicode_literals
from dnfpluginscore import _, logger

import dnf
import platform
import re

KPKG = 'kernel-core'
KPKGS = ['kernel', 'kernel-core', 'kernel-modules', 'kernel-modules-extra']
KPROVIDES = 'kernel-modules'
KPREFIX = 'kmod-*'


class KmodsPlugin(dnf.Plugin):
    name = 'kmods'

    def __init__(self, base, cli):
        super(KmodsPlugin, self).__init__(base, cli)
        self.base = base
        self.cli = cli
        self.latest_only = True
        self.filter_kernel = False
        self.filter_kmods = False
        self.filter_fast = True

    def config(self):
        conf = self.read_config(self.base.conf)
        if conf.has_section('main'):
            if conf.has_option('main', 'latest_only'):
                self.latest_only = conf.getboolean('main', 'latest_only')
        if conf.has_section('filter'):
            if conf.has_option('filter', 'kernel'):
                self.filter_kernel = conf.getboolean('filter', 'kernel')
            if conf.has_option('filter', 'kmods'):
                self.filter_kmods = conf.getboolean('filter', 'kmods')
            if conf.has_option('filter', 'fast'):
                self.filter_fast = conf.getboolean('filter', 'fast')

    def check_dependencies(self, kernel, kmod):
        kpkgs = self.base.sack.query().filter(name=KPKGS, evr=kernel.evr, arch=kernel.arch).run()
        for regex in re.compile(r"^kernel(-core|)(-uname-r|)$"), re.compile(r"^kernel\([^\s]*\)$"):
            for req in kmod.requires:
                if regex.match(req.name):
                    if not self.base.sack.query().filter(pkg=kpkgs, provides=req):
                        return False
        return True

    def resolved(self):
        kernels = self.base.sack.query().installed().filter(name=KPKG)
        kernels = kernels.union(self.base.sack.query().filter(pkg=self.base.transaction.install_set, name=KPKG))
        kernels = kernels.difference(self.base.sack.query().filter(pkg=self.base.transaction.remove_set, name=KPKG))
        if self.latest_only:
            kernels = kernels.latest().union(self.base.sack.query().installed().filter(name=KPKG, evr=platform.release().removesuffix('.{}'.format(platform.machine())), arch=platform.machine()))

        kmods = self.base.sack.query().installed().filter(name__glob=KPREFIX, provides=KPROVIDES)
        kmods = kmods.union(self.base.sack.query().filter(pkg=self.base.transaction.install_set, name__glob=KPREFIX, provides=KPROVIDES))
        kmods = kmods.difference(self.base.sack.query().filter(pkg=self.base.transaction.remove_set, name__glob=KPREFIX, provides=KPROVIDES))

        avail = self.base.sack.query().available().filter(name__glob=KPREFIX, provides=KPROVIDES).difference(kmods)

        for kmod in kmods.latest():
            for kernel in kernels.filter(arch=kmod.arch):
                warning = True
                for v in sorted(kmods.filter(name=kmod.name, arch=kmod.arch), reverse=True):
                    if self.check_dependencies(kernel, v):
                        warning = False
                        break
                if warning:
                    pkg = None
                    for v in sorted(avail.filter(name=kmod.name, arch=kmod.arch), reverse=True):
                        if self.check_dependencies(kernel, v):
                            pkg = v
                            break
                    logger.warning(_('Warning: kernel-{}.{}: kABI compatible {} not installed'.format(kernel.evr, kernel.arch, pkg if pkg else kmod.name)))

    def sack(self):
        if self.filter_kernel:
            excludes = set()
            for kmod in self.base.sack.query().installed().filter(name__glob=KPREFIX, provides=KPROVIDES).latest():
                kmodvs = sorted(self.base.sack.query().filter(name=kmod.name, arch=kmod.arch), reverse=True)
                kernels = sorted(self.base.sack.query().available().filter(name=KPKG, arch=kmod.arch), reverse=True)
                for kernel in kernels:
                    exclude = True
                    for v in kmodvs:
                        if self.check_dependencies(kernel, v):
                            exclude = False
                            break
                    if exclude:
                        logger.info(_('Info: kernel-{}.{}: Excluded due to missing compatibility update for {}'.format(kernel.evr, kernel.arch, kmod.name)))
                        excludes.add(kernel)

            for kernel in excludes:
                try:
                    self.base.sack.add_excludes(self.base.sack.query().available().filter(name=KPKGS, evr=kernel.evr))
                except Exception as error:
                    logger.error(_('Warning: sack.add_excludes error {}'.format(error)))

        if self.filter_kmods:
            if self.base.conf.best or not self.filter_fast:
                for kmod in self.base.sack.query().available().filter(name__glob=KPREFIX, provides=KPROVIDES).latest():
                    kernels = sorted(self.base.sack.query().filter(name=KPKG, arch=kmod.arch), reverse=True)
                    kmodvs = sorted(self.base.sack.query().available().filter(name=kmod.name, arch=kmod.arch), reverse=True)
                    for pkg in kmodvs:
                        exclude = True
                        for kernel in kernels:
                            if self.check_dependencies(kernel, pkg):
                                exclude = False
                                break
                        if exclude:
                            logger.debug(_('Debug: {}: Excluded due to missing compatible kernel'.format(pkg)))
                            try:
                                self.base.sack.add_excludes([pkg])
                            except Exception as error:
                                logger.error(_('Warning: sack.add_excludes error {}'.format(error)))
                        else:
                            if self.filter_fast:
                                break
